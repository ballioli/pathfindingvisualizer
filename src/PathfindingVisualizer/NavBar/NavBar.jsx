import React, { Component } from "react";
import "./NavBar.css";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      algorithm: "Dijkstra's",
    };
  }

  handleAlgorithmChange(value) {
    if (value === "aStar") value = "A*";
    else if (value === "dijkstra") value = "Dijkstra's";
    else if (value === "bfs") value = "BFS";
    else if (value === "dfs") value = "DFS";
    this.setState({ algorithm: value });
  }

  getAlgorithmInfoText(algorithm) {
    switch (algorithm) {
      case "A*":
        return "A* Search is weighted and guarantees the shortest path"
      case "Dijkstra's":
        return "Dijkstra's Algorithm is weighted and guarantees the shortest path"
      case "BFS":
        return "BFS Algorithm is unweighted and guarantees the shortest path"
      case "DFS":
        return "DFS Algorithm is unweighted and does not guarantee the shortest path"
      default:
        return "If you see this message somethings broken"
    }
  }

  render() {
    const { algorithm } = this.state;
    const {
      onAlgorithmChange,
      onVisualize,
      onSliderChange,
      onClearGrid,
      onClearPath,
      onCreateMaze,
    } = this.props;

    const infoText = this.getAlgorithmInfoText(algorithm);

    return (
      <div id="nav-container">
        <div id="nav-upper-container">
          <p id="project-name">Pathfinding Visualizer</p>
        </div>
        <div id="nav-bottom-container">
          <div className="nav-bottom-left-container flex-container">
            <div>
              <div
                onChange={() => {
                  onCreateMaze();
                }}
                className="dropdown-container"
              >
                <select name="Mazes" id="maze-dropdown" className="nav-dropdown">
                  <option select="selected">Mazes</option>
                  <option value="RecursiveDivision">RecursiveDivision's</option>
                </select>
              </div>
            </div>
            <div
              onChange={(event) => {
                this.handleAlgorithmChange(event.target.value);
                onAlgorithmChange(event.target.value);
              }}
              className="dropdown-container"
            >
              <select name="algorithms" id="algorithms-dropdown" className="nav-dropdown">
                <option value="dijkstra">Dijkstra's</option>
                <option value="aStar">A* (A Star)</option>
                <option value="bfs">BFS</option>
                <option value="dfs">DFS</option>
              </select>
            </div>
            <div>
              <label id="speed-label">Speed</label>
              <input
                type="range"
                min="20"
                max="60"
                defaultValue="60"
                className="slider"
                id="speed-slider"
                onChange={(event) => {
                  onSliderChange(event.target.value);
                }}
              ></input>
            </div>
          </div>
          <div className="nav-bottom-center-container flex-container">
            <div className="nav-item-div">
              <button
                className="nav-btn visualize-btn"
                onClick={() => onVisualize()}
              >
                Visualize {algorithm}
              </button>
            </div>
          </div>
          <div className="nav-bottom-right-container flex-container">
            <div>
              <button
                className="nav-btn clear-btn"
                onClick={() => onClearPath()}
              >
                Clear Path
              </button>
            </div>
            <div>
              <button
                className="nav-btn clear-btn"
                onClick={() => onClearGrid()}
              >
                Clear Grid
              </button>
            </div>
            <div></div>
          </div>
        </div>
        <div className="navbar-info">
          <p>{infoText}</p>
        </div>
      </div>
    );
  }
}
export default NavBar;
