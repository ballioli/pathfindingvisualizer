import React, { Component } from "react";
import door from "../../images/icons8-open-door-24.png";
import ship from "../../images/icons8-launch-24.png";
import "./Node.css";

export default class Node extends Component {
  //Unused for now, possibly needed to set active class on node elements

  render() {
    const {
      row,
      col,
      type,
      onKeyDown,
      onMouseEnter,
      cssClass,
      onKeyUp,
    } = this.props;
    const shouldRenderFinishIcon = type === "finish";
    const shouldRenderStartIcon = type === "start";
    return (
      <div
        className={`node ${type} ${cssClass}`}
        id={`node-${row}-${col}`}
        onKeyDown={(e) => onKeyDown(e)}
        onMouseEnter={() => onMouseEnter(row, col)}
        onKeyUp={() => onKeyUp()}
        tabIndex={0}
      >
        {shouldRenderFinishIcon ? (
          <div
            className="icon"
            style={{ backgroundImage: `url(${door})` }}
          ></div>
        ) : (
          <React.Fragment></React.Fragment>
        )}
        {shouldRenderStartIcon ? (
          <div
            className="icon"
            style={{ backgroundImage: `url(${ship})` }}
          ></div>
        ) : (
          <React.Fragment></React.Fragment>
        )}
      </div>
    );
  }
}
