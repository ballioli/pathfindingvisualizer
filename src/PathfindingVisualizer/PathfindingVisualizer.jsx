import React, { Component } from "react";
import { dijkstra, dijkstraShortestPath } from "../algorithms/dijkstra";
import { aStar, aStarShortestPath } from "../algorithms/astar";
import { bfs, bfsShortestPath } from "../algorithms/bfs";
import { dfs, dfsShortestPath } from "../algorithms/dfs";
import { recursiveDivision } from "../algorithms/recursiveDivision"
import Node from "./Node/Node";
import NavBar from "./NavBar/NavBar";
import Legend from "./Legend/Legend";
import "./PathfindingVisualizer.css";

const MAX_NODE_ROW = 19;
const MAX_NODE_COL = 39;

class PathfindindVisualizer extends Component {
  constructor(props) {
    super();
    this.state = {
      grid: [],
      activeNode: null, //Active not is the one currently hovered over
      startNodeCoord: { row: 6, col: 6 },
      finishNodeCoord: { row: 13, col: 33 },
      speed: 20,
      algorithm: "dijkstra",
      isWallKeyPressed: false,
      isKeyDown: false,
    };
  }
  //NOTE Keep track of which node has the active class(possibly don't need class???)

  componentDidMount() {
    const grid = getInitialGrid();
    grid[6][6].type = "start";
    grid[13][33].type = "finish";
    this.setState({ grid });
    //const test = recursiveDivision(grid);
  }

  /*
  ====================================  
  Algorithm Related functions Start
  ====================================
  */
  Visualize() {
    const { grid, startNodeCoord, finishNodeCoord, algorithm } = this.state;
    const startNode = grid[startNodeCoord.row][startNodeCoord.col];
    const finishNode = grid[finishNodeCoord.row][finishNodeCoord.col];
    let visitedNodesInOrder = [];
    let shortestPathInOrder = [];
    if (algorithm === "dijkstra") {
      visitedNodesInOrder = dijkstra(grid, startNode, finishNode);
      shortestPathInOrder = dijkstraShortestPath(finishNode);
    } else if (algorithm === "aStar") {
      visitedNodesInOrder = aStar(grid, startNode, finishNode);
      shortestPathInOrder = aStarShortestPath(finishNode);
    } else if (algorithm === "bfs") {
      visitedNodesInOrder = bfs(grid, startNode, finishNode);
      shortestPathInOrder = bfsShortestPath(finishNode);
    } else if (algorithm === "dfs") {
      visitedNodesInOrder = dfs(grid, startNode, finishNode);
      shortestPathInOrder = dfsShortestPath(finishNode);
    }
    this.animateAlgorithm(visitedNodesInOrder, shortestPathInOrder);
  }

  animateAlgorithm(visitedNodesInOrder, shortestPathInOrder) {
    if (visitedNodesInOrder === []) {
      return;
    }

    const { grid, speed } = this.state;
    //First Loop is the visitedNodes in order to visualize the pathing itself
    for (let i = 0; i < visitedNodesInOrder.length; i++) {
      //This does not run until the pathing visualization is done running
      if (i === visitedNodesInOrder.length - 1) {
        //Timeout here * i to make sure this doesn't run until pathing visualization is done
        setTimeout(() => {
          //Animate the shortest path
          this.animateShortestPath(shortestPathInOrder);
        }, speed * (i * 1.1));
      }
      //This timeout section animates the visited nodes
      //Timeout is multiplied by i to space out evenly the animations
      setTimeout(() => {
        const node = visitedNodesInOrder[i];
        const newNode = {
          ...node,
          cssClass: "visited",
        };
        grid[node.row][node.col] = newNode;
        //this.setState({ grid: grid });
        // This is to improve performance, setting state is too expensive with so many child components
        document.getElementById(`node-${node.row}-${node.col}`).className =
          "node visited";
      }, speed * i);
    }
    //this.setState({ grid: grid });
  }

  animateShortestPath(shortestPathInOrder) {
    const { grid } = this.state;
    if (shortestPathInOrder.length === 1) return;
    //Loop through the shortestPath nodes and give them an animation
    for (let i = 0; i < shortestPathInOrder.length; i++) {
      setTimeout(() => {
        const node = shortestPathInOrder[i];
        const newNode = {
          ...node,
          cssClass: "shortest-path",
        };
        grid[node.row][node.col] = newNode;
        //this.setState({ grid: grid });
        document.getElementById(`node-${node.row}-${node.col}`).className =
          "node shortest-path";
      }, 50 * i);
    }
    /* Setting timeout ~equal to the length of animating all the walls to ensure state is correctly updated */
    setTimeout(() => {
      this.setState({ grid: grid });
    }, 51 * shortestPathInOrder.length)
  }

  animateMaze(visitedNodesInOrder) {
    const { grid } = this.state;
    console.log(grid);
    if (visitedNodesInOrder.length === 0) return;

    for (let i = 0; i < visitedNodesInOrder.length; i++) {
      setTimeout(() => {
        const node = visitedNodesInOrder[i];
        const newNode = {
          ...node,
          cssClass: "maze-animation",
          type: "wall"
        };
        grid[node.row][node.col] = newNode;
        document.getElementById(`node-${node.row}-${node.col}`).className =
          "node maze-animation";
      }, 20 * i);
    }
    /* Setting timeout ~equal to the length of animating all the walls to ensure state is correctly updated */
    setTimeout(() => {
      this.setState({ grid: grid });
    }, 21 * visitedNodesInOrder.length)
  }

  /*
  ====================================  
  Algorithm Related functions End
  ====================================
  */

  /*
  ====================================  
  Handle functions Start
  ====================================
  */
  handleKeyDown(e) {
    const { startNodeCoord, finishNodeCoord, isKeyDown } = this.state;

    if (isKeyDown) return;

    //Remove row, col later if decided its not needed
    //Handle only one start and finish is allowed
    const activeRow = this.state.activeNode.row;
    const activeCol = this.state.activeNode.col;
    let newGrid = this.state.grid.slice();

    let nodeType;
    switch (e.key.toLowerCase()) {
      case "s":
        nodeType = "start";
        const oldStart = newGrid[startNodeCoord.row][startNodeCoord.col]; // Get the node that will no longer be start
        oldStart.type = "road"; // Change its type to road
        newGrid[startNodeCoord.row][startNodeCoord.col] = oldStart; //Update newGrid
        startNodeCoord.row = activeRow;
        startNodeCoord.col = activeCol;
        break;
      case "f":
        nodeType = "finish";
        const oldFinish = newGrid[finishNodeCoord.row][finishNodeCoord.col]; // Get the node that will no longer be finish
        oldFinish.type = "road"; // Change its type to road
        newGrid[finishNodeCoord.row][finishNodeCoord.col] = oldFinish; //Update newGrid
        finishNodeCoord.row = activeRow;
        finishNodeCoord.col = activeCol;
        break;
      case "w":
        this.setState({ isWallKeyPressed: true });
        nodeType = "wall";
        break;
      case "r":
        nodeType = "road";
        break;
      default:
        return;
    }

    newGrid[activeRow][activeCol].type = nodeType;

    // Update the new state
    this.setState({
      grid: newGrid,
      startNodeCoord: startNodeCoord,
      finishNodeCoord: finishNodeCoord,
      isKeyDown: true,
    });
  }

  handleMouseEnter(row, col) {
    this.setState({ activeNode: { row: row, col: col } });
    //Note state is not updated until next iteration, so inside here use row or col
    if (!this.state.isWallKeyPressed) return;
    // TODO Remove this if statement if I don't handle any other inputs here.
    if (this.state.isWallKeyPressed) {
      let newGrid = this.state.grid.slice();
      newGrid[row][col].type = "wall";
      this.setState({ grid: newGrid });
    }
  }

  handleAlgorithmChange(name) {
    this.setState({ algorithm: name });
  }

  handleVisualizeButton() {
    this.Visualize();
  }

  handleOnKeyUp() {
    this.setState({ isWallKeyPressed: false, isKeyDown: false });
  }

  handleSpeedChange(value) {
    // NOTE 40 here is the middle of the slider range i.e 20-60
    const number = Math.abs(40 - value);
    value = parseInt(value);
    if (value < 40) {
      value = value + number * 2;
    } else if (value > 40) {
      value -= number * 2;
    }
    this.setState({ speed: value });
  }

  handleClearPath() {
    const newGrid = this.state.grid.slice();
    // Clear all nodes that are not wall, re-sign finish/start node
    for (let row = 0; row <= MAX_NODE_ROW; row++) {
      for (let col = 0; col <= MAX_NODE_COL; col++) {
        const currentNode = newGrid[row][col];
        if (currentNode.type === "wall") continue;
        else if (currentNode.type === "road") {
          newGrid[row][col] = createNode(row, col);
        } else if (currentNode.type === "finish") {
          newGrid[row][col] = createNode(row, col);
          newGrid[row][col].type = "finish";
        } else if (currentNode.type === "start") {
          newGrid[row][col] = createNode(row, col);
          newGrid[row][col].type = "start";
        }
      }
    }
    this.setState({ grid: newGrid });
  }

  handleClearGrid() {
    const newGrid = getInitialGrid();
    newGrid[this.state.finishNodeCoord.row][
      this.state.finishNodeCoord.col
    ].type = "finish";
    newGrid[this.state.startNodeCoord.row][this.state.startNodeCoord.col].type =
      "start";
    this.setState({ grid: newGrid });
  }

  handleCreateMaze() {
    /* Clear the grid and setState, use callback of setState to ensure grid is cleare before animating maze */
    const newGrid = getInitialGrid();
    const mazeVisitedInOrder = recursiveDivision(newGrid);
    console.log(mazeVisitedInOrder);
    this.setState({ grid: newGrid }, this.animateMaze(mazeVisitedInOrder))
  }
  /*
  ====================================  
  Handle functions End
  ====================================
  */

  render() {
    const { grid } = this.state;
    // Create a div grid based on the grid property
    // Map through each node and create a Node component
    return (
      <div>
        <NavBar
          onAlgorithmChange={(name) => this.handleAlgorithmChange(name)}
          onVisualize={() => this.handleVisualizeButton()}
          onSliderChange={(value) => this.handleSpeedChange(value)}
          onClearPath={() => this.handleClearPath()}
          onClearGrid={() => this.handleClearGrid()}
          onCreateMaze={() => this.handleCreateMaze()} //TODO(balli): Need variable if more maze algorithm will be added
        ></NavBar>
        <Legend></Legend>
        <div className="grid">
          {grid.map((row, rowIdx) => {
            return (
              <div key={rowIdx} className="row-div">
                {row.map((node, nodeIdx) => {
                  const { row, col, type, cssClass } = node;
                  return (
                    <Node
                      key={nodeIdx}
                      row={row}
                      col={col}
                      type={type}
                      cssClass={cssClass}
                      onKeyDown={(e) => this.handleKeyDown(e)}
                      onMouseEnter={(row, col) =>
                        this.handleMouseEnter(row, col)
                      }
                      onKeyUp={() => this.handleOnKeyUp()}
                      tabIndex={0}
                    ></Node>
                  );
                })}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

/*
====================================  
Helper functions Start
====================================
*/
const getInitialGrid = () => {
  const grid = [];
  for (let row = 0; row <= MAX_NODE_ROW; row++) {
    const currentRow = [];
    for (let col = 0; col <= MAX_NODE_COL; col++) {
      currentRow.push(createNode(row, col));
    }
    grid.push(currentRow);
  }
  return grid;
};

const getUpdatedWalls = (row, col) => {
  let newGrid = this.state.grid.slice();
  newGrid[row][col].type = "wall";
  return newGrid;
};

const createNode = (row, col) => {
  let nodeType = "road";

  return {
    row,
    col,
    distance: Infinity,
    previousNode: null,
    isVisited: false,
    type: nodeType, //road, wall, start, finish
    cssClass: "",
    hCost: Infinity,
    mazeWallCheck: false, // This if for maze algorithms to check for walls without setting type
  };
};
/*
====================================  
Helper functions End
====================================
*/
export default PathfindindVisualizer;
