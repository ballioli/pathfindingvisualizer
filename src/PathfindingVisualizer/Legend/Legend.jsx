import "./Legend.css";
import React from "react";
import door from "../../images/icons8-open-door-24.png";
import ship from "../../images/icons8-launch-24.png";

const Legend = (props) => {
  return (
    <div className="legend-div">
      <div className="legend-container">
        <div className="start-node legend-item">
          <div className="node-legend">
            <div
              className="legend-icon"
              style={{ backgroundImage: `url(${ship})` }}
            ></div>
          </div>
          <div className="legend-text">Start Node</div>
        </div>
        <div className="start-node legend-item">
          <div className="node-legend">
            <div
              className="legend-icon"
              style={{ backgroundImage: `url(${door})` }}
            ></div>
          </div>
          <div className="legend-text">Target Node</div>
        </div>
        <div className="start-node legend-item">
          <div className="node-legend"></div>
          <div className="legend-text">Unvisited Note</div>
        </div>
        <div className="start-node legend-item">
          <div className="node-legend" id="visited-1"></div>
          <div className="node-legend" id="visited-2"></div>
          <div className="node-legend" id="visited-3"></div>
          <div className="legend-text">Visited Notes</div>
        </div>
        <div className="start-node legend-item">
          <div className="node-legend" id="wall"></div>
          <div className="legend-text">Wall Note</div>
        </div>
        <div className="start-node legend-item">
          <div className="node-legend" id="shortest-path"></div>
          <div className="legend-text">Shortest-Path Note</div>
        </div>
      </div>
    </div>
  );
};

export default Legend;
