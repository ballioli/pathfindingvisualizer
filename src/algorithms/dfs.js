// Guarantees shortest path in a weighted graph

export function dfs(grid, startNode, finishNode) {
  // Return an array of visited nodes in order

  const visitedNodesInOrder = [];
  const stack = [];
  stack.push(startNode);
  while (stack.length > 0) {
    const closestNode = stack.pop(); // Remove the first element

    // If node is a wall ignore it
    if (closestNode.type === "wall") continue;

    if (!closestNode.isVisited) {
      visitedNodesInOrder.push(closestNode);
      closestNode.isVisited = true;
    }

    if (closestNode === finishNode) return visitedNodesInOrder;
    updateNeighbours(closestNode, grid, stack);
  }
  return visitedNodesInOrder;
}

function updateNeighbours(node, grid, stack) {
  // Get this nodes neighbours that have not been visited
  const unvisitedNeighbours = getUnvisitedNeighbours(node, grid);
  for (const neighbour of unvisitedNeighbours) {
    neighbour.previousNode = node;
    stack.push(neighbour);
  }
}

function getUnvisitedNeighbours(node, grid) {
  const neighbours = [];
  const { row, col } = node;
  if (col > 0) neighbours.push(grid[row][col - 1]); // LEFT
  if (row < grid.length - 1) neighbours.push(grid[row + 1][col]); // DOWN
  if (col < grid[0].length - 1) neighbours.push(grid[row][col + 1]); // Right
  if (row > 0) neighbours.push(grid[row - 1][col]); // UP
  return neighbours.filter((neighbour) => !neighbour.isVisited);
}

export function dfsShortestPath(finishNode) {
  // Backtrack to the starting node
  const shortestPathOfNodes = [];
  let currentNode = finishNode;
  while (currentNode !== null) {
    shortestPathOfNodes.unshift(currentNode);
    currentNode = currentNode.previousNode;
  }
  return shortestPathOfNodes;
}
