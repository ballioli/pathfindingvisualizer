// Guarantees shortest path in an unweigthed graph

export function bfs(grid, startNode, finishNode) {
  // Return an array of visited nodes in order

  const visitedNodesInOrder = [];
  const queue = [];
  queue.push(startNode);
  while (queue.length > 0) {
    console.log(queue.length);
    const closestNode = queue.shift(); // Remove the first element

    // If node is a wall ignore it
    if (closestNode.type === "wall") continue;

    closestNode.isVisited = true;
    visitedNodesInOrder.push(closestNode);

    if (closestNode === finishNode) return visitedNodesInOrder;
    updateNeighbours(closestNode, grid, queue);
  }
  return visitedNodesInOrder;
}

function updateNeighbours(node, grid, queue) {
  // Get this nodes neighbours that have not been visited
  const unvisitedNeighbours = getUnvisitedNeighbours(node, grid);
  for (const neighbour of unvisitedNeighbours) {
    neighbour.previousNode = node;
    neighbour.isVisited = true;
    queue.push(neighbour);
  }
}

function getUnvisitedNeighbours(node, grid) {
  const neighbours = [];
  const { row, col } = node;
  if (row > 0) neighbours.push(grid[row - 1][col]);
  if (row < grid.length - 1) neighbours.push(grid[row + 1][col]);
  if (col > 0) neighbours.push(grid[row][col - 1]);
  if (col < grid[0].length - 1) neighbours.push(grid[row][col + 1]);
  return neighbours.filter((neighbour) => !neighbour.isVisited);
}

export function bfsShortestPath(finishNode) {
  // Backtrack to the starting node
  const shortestPathOfNodes = [];
  let currentNode = finishNode;
  while (currentNode !== null) {
    shortestPathOfNodes.unshift(currentNode);
    currentNode = currentNode.previousNode;
  }
  return shortestPathOfNodes;
}
