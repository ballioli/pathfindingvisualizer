// Guarantees shortest path in a weighted graph + uses heuristic for optimization

export function aStar(grid, startNode, finishNode) {
  startNode.distance = 0;
  const visitedNodesInOrder = [];
  const unvisitedNodes = getNodesFromGrid(grid);
  var firstIteration = true;
  while (unvisitedNodes.length > 0) {
    var closestNode = null;
    if (firstIteration) {
      closestNode = startNode;
      const index = unvisitedNodes.indexOf(startNode);
      firstIteration = false;
      unvisitedNodes.splice(index, 1);
    } else {
      sortNodesByDistance(unvisitedNodes);
      closestNode = unvisitedNodes.shift(); //Remove the first element
    }

    // If node is a wall ignore it
    if (closestNode.type === "wall") continue;
    // If nodes distance is infinity there is no path to finish
    if (closestNode.distance === Infinity) return visitedNodesInOrder;
    closestNode.isVisited = true;
    visitedNodesInOrder.push(closestNode);

    if (closestNode === finishNode) return visitedNodesInOrder;
    updateNeighboursDistanceAndHeuristics(closestNode, grid, finishNode);
  }
}

function updateNeighboursDistanceAndHeuristics(node, grid, finishNode) {
  // Get this nodes neighbours that have not been visited
  const unvisitedNeighbours = getUnvisitedNeighbours(node, grid);
  for (const neighbour of unvisitedNeighbours) {
    // Calculate heuristics and distance
    const rowDistance = Math.abs(finishNode.row - neighbour.row);
    const colDistance = Math.abs(finishNode.col - neighbour.col);
    neighbour.hCost = rowDistance + colDistance;
    neighbour.distance = node.distance + 1;
    neighbour.previousNode = node;
  }
}

function getNodesFromGrid(grid) {
  const nodes = [];
  for (const row of grid) {
    for (const node of row) {
      nodes.push(node);
    }
  }
  return nodes;
}

function getUnvisitedNeighbours(node, grid) {
  const neighbours = [];
  const { row, col } = node;
  if (row > 0) neighbours.push(grid[row - 1][col]);
  if (row < grid.length - 1) neighbours.push(grid[row + 1][col]);
  if (col > 0) neighbours.push(grid[row][col - 1]);
  if (col < grid[0].length - 1) neighbours.push(grid[row][col + 1]);
  return neighbours.filter((neighbour) => !neighbour.isVisited);
}

function sortNodesByDistance(nodes) {
  // Sort via Astar f(n) = g(n) + h(n)
  // Distance + Heuristics
  nodes.sort((a, b) => a.distance + a.hCost - (b.distance + b.hCost));
}

export function aStarShortestPath(finishNode) {
  // Backtrack to the starting node
  const shortestPathOfNodes = [];
  let currentNode = finishNode;
  while (currentNode !== null) {
    shortestPathOfNodes.unshift(currentNode);
    currentNode = currentNode.previousNode;
  }
  return shortestPathOfNodes;
}
