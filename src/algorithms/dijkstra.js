// Guarantees shortest path in a weighted graph

export function dijkstra(grid, startNode, finishNode) {
  // Return an array of visited nodes in order
  startNode.distance = 0;
  const visitedNodesInOrder = [];
  const unvisitedNodes = getNodesFromGrid(grid);
  while (unvisitedNodes.length > 0) {
    sortNodesByDistance(unvisitedNodes);
    const closestNode = unvisitedNodes.shift(); // Remove the first element

    // If node is a wall ignore it
    if (closestNode.type === "wall") continue;

    // If nodes distance is infinity there is no path to finish
    if (closestNode.distance === Infinity) return visitedNodesInOrder;
    closestNode.isVisited = true;
    visitedNodesInOrder.push(closestNode);

    if (closestNode === finishNode) return visitedNodesInOrder;
    updateNeighboursDistance(closestNode, grid);
    // If not update the closest nodes distance(here we also update their previous node property)
  }
}

function updateNeighboursDistance(node, grid) {
  // Get this nodes neighbours that have not been visited
  const unvisitedNeighbours = getUnvisitedNeighbours(node, grid);
  for (const neighbour of unvisitedNeighbours) {
    neighbour.distance = node.distance + 1;
    neighbour.previousNode = node;
  }
}

function getUnvisitedNeighbours(node, grid) {
  const neighbours = [];
  const { row, col } = node;
  if (row > 0) neighbours.push(grid[row - 1][col]);
  if (row < grid.length - 1) neighbours.push(grid[row + 1][col]);
  if (col > 0) neighbours.push(grid[row][col - 1]);
  if (col < grid[0].length - 1) neighbours.push(grid[row][col + 1]);
  return neighbours.filter((neighbour) => !neighbour.isVisited);
}

function sortNodesByDistance(nodes) {
  nodes.sort((a, b) => a.distance - b.distance);
}

function getNodesFromGrid(grid) {
  const nodes = [];
  for (const row of grid) {
    for (const node of row) {
      nodes.push(node);
    }
  }
  return nodes;
}

export function dijkstraShortestPath(finishNode) {
  // Backtrack to the starting node
  const shortestPathOfNodes = [];
  let currentNode = finishNode;
  while (currentNode !== null) {
    shortestPathOfNodes.unshift(currentNode);
    currentNode = currentNode.previousNode;
  }
  return shortestPathOfNodes;
}
