export function recursiveDivision(grid) {
    const visitedNotesInOrder = [];
    /* Start by filling the edges of the grid */
    fillEdgesOfGrid(grid, visitedNotesInOrder);
    /* Recursively divide and create walls */
    divide(grid, visitedNotesInOrder, 1, 1, 38, 18, "Vertical");
    return visitedNotesInOrder;
}


function divide(grid, visitedNotesInOrder, x, y, width, height, orientation) {
    /* Base case */
    if (width < 2 || height < 2) return;

    if (orientation === "Horizontal") {
        /* Draw Horizontal wall with a gap */

        /* Fetch all viable indexes according to rules(i.e don't close doors or paint walls next to another) */
        const viableIndexes = findViableIndexes(orientation, grid, x, y, width, height);

        /* Get a random index from the list of viable indexes */
        let yIndex = viableIndexes[randomNumber(viableIndexes.length)];
        /* If there are no viable indexes return */
        if (viableIndexes.length < 1) return;

        /* Line variable is used to calculate the height of both divisions */
        let line = yIndex - y;
        /* Random where a gap in the wall should be */
        const gap = randomNumber(width);
        /* Looping through the row setting its type variable and adding to a list to be later animated */
        for (let i = x; i < x + width; i++) {
            const node = grid[yIndex][i];
            if (node.col === gap + x) continue;
            node.mazeWallCheck = true;
            visitedNotesInOrder.push(node);
        }
        /* Calculate new heights and Y value */
        // NOTE x and y are always in the top left corner of new division
        const newHeight = height - line - 1;
        const newY = yIndex + 1;
        divide(grid, visitedNotesInOrder, x, y, width, line, getOrientation(width, line));
        divide(grid, visitedNotesInOrder, x, newY, width, newHeight, getOrientation(width, newHeight));
    } else {
        /* Draw Vertical wall with a gap */

        /* Fetch all viable indexes according to rules(i.e don't close doors or paint walls next to another) */
        const viableIndexes = findViableIndexes(orientation, grid, x, y, width, height);

        /* Get a random index from the list of viable indexes */
        let xIndex = viableIndexes[randomNumber(viableIndexes.length)];
        /* If there are no viable indexes return */
        if (viableIndexes.length < 1) return;
        /* Line variable is used to calculate the width of both divisions */
        let line = xIndex - x;
        /* Random where a gap in the wall should be */
        const gap = randomNumber(height);


        /* Looping through the columns setting its type variable and adding to a list to be later animated */
        for (let i = y; i < y + height; i++) {
            const node = grid[i][xIndex];
            if (node.row === gap + y) continue;
            node.mazeWallCheck = true;
            visitedNotesInOrder.push(node);
        }
        /* Calculate new width and x value */
        // NOTE x and y are always in the top left corner of new division
        const newWidth = width - line - 1;
        const newX = xIndex + 1;
        divide(grid, visitedNotesInOrder, x, y, line, height, getOrientation(line, height))
        divide(grid, visitedNotesInOrder, newX, y, newWidth, height, getOrientation(newWidth, height))
    }
}

function findViableIndexes(orientation, grid, x, y, width, height) {

    const viableIndexes = [];
    if (orientation === "Horizontal") {
        /* Check if on both top and bottom of current division there are wall
        *  If there is a road on either side that means that index is not viable option
        *  as it would potentially close the maze
        *  also don't check the first and last index as walls can not be drawn next to another wall
        */
        for (let i = y + 1; i < y + height - 1; i++) {
            if (grid[i][x - 1].mazeWallCheck && grid[i][x + width].mazeWallCheck) {
                viableIndexes.push(i);
            }
        }
    }
    else {
        /* Same deal as above except check left/right wall instead top/bottom */
        for (let i = x + 1; i < x + width - 1; i++) {
            if (grid[y - 1][i].mazeWallCheck && grid[y + height][i].mazeWallCheck) {
                viableIndexes.push(i);
            }
        }
    }
    return viableIndexes;
}

function getOrientation(width, height) {
    /* Get which orientation should be drawn based on current divisions width and height */
    if (width < height) {
        return "Horizontal"
    }
    else if (height < width) {
        return "Vertical"
    } else {
        return randomNumber(2) === 0 ? "Horizontal" : "Vertical";
    }
}

function randomNumber(max) {
    //Random number from 0...max - 1
    //randomNumber(3) random 0, 1 or 2
    return Math.floor(Math.random() * Math.floor(max));
}

function fillEdgesOfGrid(grid, visitedNodesInOrder) {
    /* Fill top and bottom row */
    for (let i = 0; i <= grid.length; i = i + grid.length - 1) {
        for (let j = 0; j < grid[i].length; j++) {
            const node = grid[i][j];
            node.mazeWallCheck = true;
            visitedNodesInOrder.push(node);
        }
    }
    /* Fill first and last column */
    for (let i = 0; i <= grid[0].length; i = i + grid[0].length - 1) {
        for (let j = 0; j < grid.length; j++) {
            if (j === 0 || j === grid.length - 1) continue;
            const node = grid[j][i];
            node.mazeWallCheck = true;
            visitedNodesInOrder.push(node);
        }
    }
}