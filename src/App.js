import React from "react";
//import logo from './logo.svg';
import PathfindingVisualizer from "./PathfindingVisualizer/PathfindingVisualizer";
import "./App.css";

function App() {
  return (
    <div className="App">
      <PathfindingVisualizer></PathfindingVisualizer>
      <div>Icons by Icon8</div>
    </div>
  );
}

export default App;
