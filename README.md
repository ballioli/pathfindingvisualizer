# Pathfinding Visualizer

Note: This project is still a work in progress and is not complete

### Installation

* Install latest version of Node
* Run `npm install` in the `pathfinding-visualizer` directory
* Run `npm start`

### Usage
* Click grid
* Hover over note you want to change
* Hit the following hotkeys
* s = start Node
* f = target Node
* w = wall Node
* r = road Node

### Gif Examples

#### Maze Generation, Recursive Division

![Alt Text](https://media.giphy.com/media/SfCCULssPgY5T7Pqzi/giphy.gif)

#### Dijkstra's

![Alt Text](https://media.giphy.com/media/6rkxwuBJ1P838gAHXn/giphy.gif)

#### A* (A Star)

![Alt Text](https://media.giphy.com/media/tobzniGUXn69x7PC0b/giphy.gif)

#### DFS

![Alt Text](https://media.giphy.com/media/Ewmy9kko35sHSVjV78/giphy.gif)
